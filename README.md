## Android Merge Adapter ##

Create List with multiple adapter  
Download apk file [here](https://www.dropbox.com/s/n932jasfeo9gyqs)

#### Screenshot ####
| 1 |
| :---: |
| ![](https://images2.imgbox.com/30/a2/IUJ4nTbK_o.png) |

#### List Library ####
- [Glide](https://github.com/bumptech/glide)

#### References ####
- [Medium](https://medium.com/androiddevelopers/merge-adapters-sequentially-with-mergeadapter-294d2942127a)
- [Mindorks](https://blog.mindorks.com/implementing-merge-adapter-in-android-tutorial)