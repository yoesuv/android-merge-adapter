package com.yoesuv.mymergeadapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.MergeAdapter
import com.yoesuv.mymergeadapter.adapters.MyTextAdapter
import com.yoesuv.mymergeadapter.adapters.PersonAdapter
import com.yoesuv.mymergeadapter.databinding.ActivityMainBinding
import com.yoesuv.mymergeadapter.utis.DataSource

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var myTextAdapter: MyTextAdapter
    private lateinit var personAdapter: PersonAdapter
    private lateinit var mergeAdapter: MergeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        val lManager = LinearLayoutManager(this)

        myTextAdapter = MyTextAdapter()
        myTextAdapter.submitList(DataSource.getMyText())
        personAdapter = PersonAdapter()
        personAdapter.submitList(DataSource.getPerson())
        mergeAdapter = MergeAdapter(myTextAdapter, personAdapter)

        binding.rvMain.apply {
            layoutManager = lManager
            adapter = mergeAdapter
        }
    }
}