package com.yoesuv.mymergeadapter.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yoesuv.mymergeadapter.databinding.ItemTextBinding
import com.yoesuv.mymergeadapter.models.MyText
import com.yoesuv.mymergeadapter.utis.AdapterCallback

class MyTextAdapter: ListAdapter<MyText, MyTextAdapter.MyTextViewHolder>(AdapterCallback.DIFF_MY_TEXT) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyTextViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemTextBinding = ItemTextBinding.inflate(inflater, parent, false)
        return MyTextViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyTextViewHolder, position: Int) {
        holder.bindData(getItem(position))
    }

    class MyTextViewHolder(private val binding: ItemTextBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindData(myText: MyText) {
            binding.textViewItemText.text = myText.content
        }
    }
}