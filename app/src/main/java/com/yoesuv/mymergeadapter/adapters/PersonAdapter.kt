package com.yoesuv.mymergeadapter.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yoesuv.mymergeadapter.databinding.ItemPersonBinding
import com.yoesuv.mymergeadapter.models.Person
import com.yoesuv.mymergeadapter.utis.AdapterCallback
import com.yoesuv.mymergeadapter.utis.GlideApp

class PersonAdapter: ListAdapter<Person, PersonAdapter.PersonViewHolder>(AdapterCallback.DIFF_PERSON) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemPersonBinding = ItemPersonBinding.inflate(inflater, parent, false)
        return PersonViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        holder.bindData(getItem(position))
    }

    class PersonViewHolder(private val binding: ItemPersonBinding): RecyclerView.ViewHolder(binding.root) {
        fun bindData(person: Person) {
            binding.tvPersonName.text = person.name
            GlideApp.with(binding.ivPerson)
                .load(person.avatar)
                .into(binding.ivPerson)
        }
    }
}