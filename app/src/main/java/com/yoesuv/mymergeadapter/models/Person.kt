package com.yoesuv.mymergeadapter.models

data class Person (
    val name: String,
    val avatar: String
)