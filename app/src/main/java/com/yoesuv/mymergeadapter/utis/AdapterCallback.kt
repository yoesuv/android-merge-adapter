package com.yoesuv.mymergeadapter.utis

import androidx.recyclerview.widget.DiffUtil
import com.yoesuv.mymergeadapter.models.MyText
import com.yoesuv.mymergeadapter.models.Person

object AdapterCallback {

    val DIFF_MY_TEXT = object : DiffUtil.ItemCallback<MyText>() {
        override fun areItemsTheSame(oldItem: MyText, newItem: MyText): Boolean {
            return oldItem == newItem
        }
        override fun areContentsTheSame(oldItem: MyText, newItem: MyText): Boolean {
            return oldItem.content == newItem.content
        }
    }

    val DIFF_PERSON = object : DiffUtil.ItemCallback<Person>() {
        override fun areItemsTheSame(oldItem: Person, newItem: Person): Boolean {
            return oldItem == newItem
        }
        override fun areContentsTheSame(oldItem: Person, newItem: Person): Boolean {
            return oldItem.name == newItem.name
        }
    }

}