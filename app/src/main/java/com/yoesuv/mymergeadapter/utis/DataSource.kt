package com.yoesuv.mymergeadapter.utis

import com.yoesuv.mymergeadapter.models.MyText
import com.yoesuv.mymergeadapter.models.Person

object DataSource {

    fun getMyText(): MutableList<MyText> {
        return arrayListOf(
            MyText("Alex Rins"),
            MyText("Andrea Dovisioso"),
            MyText("Fabio Quartararo"),
            MyText("Jack Miller"),
            MyText("Marc Marquez"),
            MyText("Maveric Vinales"),
            MyText("Valentino Rossi")
        )
    }

    fun getPerson(): MutableList<Person> {
        return arrayListOf(
            Person("Alisson Beker","https://timesofindia.indiatimes.com/thumb/msid-65077748,imgsize-426807,width-400,resizemode-4/65077748.jpg"),
            Person("Bale","https://cdn.images.dailystar.co.uk/dynamic/58/photos/858000/620x/Gareth-Bale-668971.jpg"),
            Person("Christiano Ronaldo","https://images.performgroup.com/di/library/GOAL/b1/6d/cristiano-ronaldo-juventus-2018-19_pg80rqcogi91wu988vrj0fz6.jpg?t=-851553187&quality=90&h=300"),
            Person("David Silva", "https://media.vivagoal.com/2019/08/David-SIlva.jpg"),
            Person("Eden Hazard","https://s.hs-data.com/bilder/spieler/gross/87809.jpg"),
            Person("Firminho","https://vignette.wikia.nocookie.net/liverpoolfc/images/f/f3/RFirmino2019.jpeg/revision/latest?cb=20190807042207"),
            Person("Gianluigi Buffon","https://secure.cache.images.core.optasports.com/soccer/players/150x150/53.png"),
            Person("Hector Bellerin","https://cdn.idntimes.com/content-images/community/2020/02/hectorbellerin-20200201-185830-0-e4e6362d057247c69610f90d791b1619.jpg"),
            Person("Isco Alarcon","https://cdn.images.dailystar.co.uk/dynamic/58/photos/984000/620x/53cfaa031adcd_461021885.jpg"),
            Person("Jamie Vardy", "https://gilabola.com/wp-content/uploads/2020/02/pep-guardiola-jamie-vardy-696x522.jpeg")
        )
    }
}